# RealEstate

[Click here](https://real-estate-767af.firebaseapp.com) to access running application.

Initial Users

| Email                  | Password  | Role    |
|------------------------|-----------|---------|
| bgokselburak@gmail.com | 123456789 | Admin   |
| gokselb@icloud.com     | 123456789 | Realtor |

Initial role of account is Client. Can be changed by an Admin.


## Run application

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.