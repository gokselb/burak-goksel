export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC2Mmv2oWM3rsv_rRydgvhfxH2NmgkB6vk',
    authDomain: 'real-estate-767af.firebaseapp.com',
    databaseURL: 'https://real-estate-767af.firebaseio.com',
    projectId: 'real-estate-767af',
    storageBucket: 'real-estate-767af.appspot.com',
    messagingSenderId: '972875356312',
    appId: '1:972875356312:web:76999fdff58946c01658ed'
  }
};
