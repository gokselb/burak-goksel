// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC2Mmv2oWM3rsv_rRydgvhfxH2NmgkB6vk',
    authDomain: 'real-estate-767af.firebaseapp.com',
    databaseURL: 'https://real-estate-767af.firebaseio.com',
    projectId: 'real-estate-767af',
    storageBucket: 'real-estate-767af.appspot.com',
    messagingSenderId: '972875356312',
    appId: '1:972875356312:web:76999fdff58946c01658ed'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
