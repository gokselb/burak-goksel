import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';

// Shared
import { AuthService, NavService } from '@re-shared/services';
import { User, Nav, Roles } from '@re-shared/models';

@Injectable()
export class ReAuthGuard implements CanActivate {
  private user: User;
  private navs: Nav[];

  public constructor(
    private authService: AuthService,
    private navService: NavService,
    private router: Router
  ) {
    this.watchChanges();
  }

  public canActivate(route: ActivatedRouteSnapshot): boolean {
    const realtedNav = this.navs.find(nav => nav.url.indexOf(route.url.join('')) !== -1);

    if (!realtedNav) {
      this.router.navigateByUrl('');
      return false;
    }

    const allowed =
      (this.user && realtedNav.roles.indexOf(this.user.role) > -1) || (!this.user && realtedNav.roles.indexOf(Roles.Client) > -1);
    if (!allowed) {
      this.router.navigateByUrl('');
    }
    return allowed;
  }

  private watchChanges(): void {
    this.authService.user.subscribe(user => this.user = user);
    this.navService.navs.subscribe(navs => this.navs = navs);
  }
}
