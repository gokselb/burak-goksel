import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';

// 3rd
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

// Shared
import { AuthService, NotifierService } from '@re-shared/services';

@Injectable()
export class ReHttpInterceptor implements HttpInterceptor {
  constructor(
    private auth: AuthService,
    private notifier: NotifierService
  ) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    if (request.url.indexOf('accounts:') === -1 && token) {
      request = request.clone({
        setParams: {
          auth: token
        }
      });
      // request.params.set('auth', localStorage.getItem('token'));
    }

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse, caught) => {
        if (err.status === 401) {
          this.auth.logout();
          this.notifier.error('Your session has timed out. Please login again.');
          setTimeout(() => {
            location.reload();
          }, 600);
        }
        return throwError(err);
      })
    );
  }
}
