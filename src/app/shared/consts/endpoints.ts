export const Endpoints = {
  SigninWithPassword: 'accounts:signInWithPassword',
  SignUp: 'accounts:signUp',
  SendOObCode: 'accounts:sendOobCode',
  Lookup: 'accounts:lookup',
  Delete: 'accounts:delete',
  Update: 'accounts:update'
}