import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// 3rd
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

// Local
import { ServicesModule } from './services/services.module';
import { ComponentsModule } from './components/components.module';
import { ReHttpInterceptor } from './security/http.interceptor';
import { ReAuthGuard } from './security/auth.guard';

@NgModule({
  imports: [
    ServicesModule,
    ComponentsModule,
    NgbModalModule
  ],
  declarations: [
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ReHttpInterceptor,
      multi: true
    },
    ReAuthGuard
  ],
  exports: [
    ComponentsModule
  ]
})
export class SharedModule { }
