import { Component, Input } from '@angular/core';

// 3rd
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

// Shared
import { Property } from '@re-shared/models';

@Component({
  selector: 'app-edit-property-modal',
  templateUrl: './edit-property-modal.component.html'
})

export class EditPropertyModalComponent {
  @Input() public property: Property;
  public modalRef: NgbModalRef;
  public formValid = false;
  public constructor() { }
}
