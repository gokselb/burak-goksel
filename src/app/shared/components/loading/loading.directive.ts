import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[loading]',
})
export class LoadingDirective {

  private nativeElement: HTMLElement;

  public constructor(private el: ElementRef) {
    this.nativeElement = el.nativeElement;
  }

  @Input() set loading(loading: boolean) {
    if (loading) {
      this.appendHtml();
    } else {
      this.removeHtml();
    }
  }
  private appendHtml(): void {
    this.nativeElement.insertAdjacentHTML('beforeend', '<div class="re-loading"></div>');
  }
  private removeHtml(): void {
    const loading = this.nativeElement.getElementsByClassName('re-loading');
    if (loading.length === 1) {
      loading[0].remove();
    }
  }
}
