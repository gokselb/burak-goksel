import { Component, Input } from '@angular/core';

// Shared
import { User } from '@re-shared/models';

@Component({
  selector: 'app-user-tag',
  templateUrl: './user-tag.component.html'
})
export class UserTagComponent {
  @Input() public user: User;
}
