import { Pipe, PipeTransform } from '@angular/core';

// Shared
import { User } from '@re-shared/models';

@Pipe({
  name: 'userSearch',
  pure: false
})
export class UserSearchPipe implements PipeTransform {
  transform(items: User[], search: string): any {
    if (!items || !search) {
      return items;
    }
    return items.filter(item =>
      item.name.toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) > -1 ||
      item.email.toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) > -1
    );
  }
}
