import { Component, Input } from '@angular/core';
import { User } from '@re-shared/models';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit-user-modal',
  templateUrl: './edit-user-modal.component.html'
})

export class EditUserModalComponent {
  @Input() public user: User;
  public modalRef: NgbModalRef;
  public formValid = false;
  public constructor() { }
}
