import { Component, Input } from '@angular/core';

// Shared
import { User, Roles } from '@re-shared/models';
import { AuthService } from '@re-shared/services';
import { ModalService } from '@re-shared/services/modal.service';
import { EditUserModalComponent } from '../edit-user/edit-user-modal.component';
import { UserApiService } from '@re-shared/services/api';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html'
})
export class UserCardComponent {
  @Input() public user: User;
  public deleted = false;
  public loading = false;
  public cudEnabled = false;
  public currentUser: User;

  public constructor(
    private authService: AuthService,
    private modalService: ModalService
  ) {
    this.getCurrentUser();
  }

  public deleteUser(): void {
    this.modalService.showConfirmationModal(`This user will be deleted permanently: ${this.user.name}?`).result.then(
      val => {
        if (val) {
          this.loading = true;
          this.authService.deleteUser(this.user).subscribe(result => {
            if (result) {
              this.deleted = true;
            }
          },
            () => { this.loading = false; }
          );
        }
      },
      () => { }
    );
  }

  public editUser(): void {
    const ref = this.modalService.show(EditUserModalComponent);
    ref.componentInstance.user = Object.assign({}, this.user);
    ref.result.then(
      (val: User) => {
        if (this.user !== val) {
          this.loading = true;
          this.authService.updateUser(this.user, val).subscribe(
            user => {
              this.user = user;
              this.loading = false;
            },
            () => { },
            () => {
              this.loading = false;
            }
          );
        }
      },
      () => { }
    );
  }

  public resetPassword(): void {
    this.loading = true;
    this.authService.resetPassword(this.user).subscribe(
      () => { this.loading = false; },
      () => { this.loading = false; }
    );
  }

  private getCurrentUser(): void {
    this.authService.user.subscribe(val => {
      this.currentUser = val;
      this.cudEnabled = val && val.role === Roles.Admin;
    });
  }
}
