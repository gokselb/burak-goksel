import { Component, Input, OnInit } from '@angular/core';

// Shared
import { Property, Roles, State } from '@re-shared/models';
import { AuthService, NotifierService, ModalService, PropertyService } from '@re-shared/services';
import { EditPropertyModalComponent } from '../edit-property/edit-property-modal.component';

@Component({
  selector: 'app-property-card',
  templateUrl: './property-card.component.html'
})
export class PropertyCardComponent implements OnInit {
  @Input() public property: Property;
  public cudEnabled = false;
  public loading = false;
  public deleted = false;

  public constructor(
    private authService: AuthService,
    private propertyService: PropertyService,
    private modalService: ModalService,
    private notifier: NotifierService
  ) {
  }

  public ngOnInit(): void {
    this.getCurrentUser();
  }

  public deleteProperty(): void {
    this.modalService.showConfirmationModal(`This property will be deleted permanently: ${this.property.name}?`).result.then(
      val => {
        if (val) {
          this.loading = true;
          this.propertyService.delete(this.property.id).subscribe(result => {
            if (result) {
              this.deleted = true;
            }
          },
            () => { this.loading = false; }
          );
        }
      },
      () => { }
    );
  }

  public editProperty(): void {
    const ref = this.modalService.show(EditPropertyModalComponent);
    ref.componentInstance.property = Object.assign({}, this.property);
    ref.result.then(
      (val: Property) => {
        if (this.property !== val) {
          this.loading = true;
          this.propertyService.update(val).subscribe(
            property => {
              this.property = property;
              this.loading = false;
            },
            () => null,
            () => {
              this.loading = false;
            }
          );
        }
      },
      () => { }
    );
  }

  public changeStatus(): void {
    this.loading = true;
    const changeState = () => {
      if (this.property.state === State.Available) {
        this.property.state = State.Rented;
        this.property.dateRented = new Date();
      } else {
        this.property.state = State.Available;
        this.property.dateRented = null;
      }
    };
    changeState();
    this.propertyService.update(this.property, false).subscribe(
      () => { this.notifier.success('Status changed successfully.'); },
      () => {
        changeState();
        this.notifier.error();
      },
      () => this.loading = false
    );
  }

  private getCurrentUser(): void {
    this.authService.user.subscribe(val => {
      this.cudEnabled = val && val.role === Roles.Admin || val && this.property && val.id === this.property.realtor.id;
    });
  }
}
