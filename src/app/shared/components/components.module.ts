import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// Local
import { LoginFormComponent } from './login-form/login/login-form.component';
import { LoginLayoutComponent } from './login-form/login-form-layout.component';
import { RegisterFormComponent } from './login-form/register/register-form.component';
import { MapSmallComponent } from './map-small/map-small.component';
import { UserTagComponent } from './user-tag/user-tag.component';
import { LoadingDirective } from './loading/loading.directive';
import { CreateButtonComponent } from './create-button/create-button.component';
import { UserCardComponent } from './user-card/user-card.component';
import { UserFormComponent } from './forms/user-form.component';
import { PropertyFormComponent } from './forms/property-form.component';
import { EditUserModalComponent } from './edit-user/edit-user-modal.component';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';
import { LocationPickerComponent } from './location-picker/location-picker.component';
import { PropertyCardComponent } from './property-card/property-card.component';
import { EditPropertyModalComponent } from './edit-property/edit-property-modal.component';
import { UserSearchPipe } from './user-search/user-search.pipe';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
  ],
  declarations: [
    LoginLayoutComponent,
    LoginFormComponent,
    RegisterFormComponent,
    MapSmallComponent,
    LocationPickerComponent,
    UserTagComponent,
    PropertyCardComponent,
    CreateButtonComponent,
    UserCardComponent,
    LoadingDirective,
    UserFormComponent,
    PropertyFormComponent,
    EditUserModalComponent,
    EditPropertyModalComponent,
    ConfirmationModalComponent,
    UserSearchPipe
  ],
  exports: [
    LoginLayoutComponent,
    LoginFormComponent,
    RegisterFormComponent,
    MapSmallComponent,
    LocationPickerComponent,
    UserTagComponent,
    PropertyCardComponent,
    CreateButtonComponent,
    UserCardComponent,
    LoadingDirective,
    UserFormComponent,
    PropertyFormComponent,
    EditUserModalComponent,
    EditPropertyModalComponent,
    ConfirmationModalComponent,
    UserSearchPipe
  ],
  entryComponents: [
    EditUserModalComponent,
    EditPropertyModalComponent,
    ConfirmationModalComponent
  ],
  providers: []
})
export class ComponentsModule { }
