import { Component, ViewChild, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

// 3rd
import { } from 'googlemaps';
import { GeoLocation } from '@re-shared/models';
import { MapOptions } from '@re-shared/consts';

@Component({
  selector: 'app-map-small',
  templateUrl: './map-small.component.html'
})
export class MapSmallComponent implements OnInit, OnChanges {
  @Input() geoLocation: GeoLocation;
  @ViewChild('map', { static: true }) mapElement: any;
  map: google.maps.Map;
  private marker: google.maps.Marker;

  public ngOnInit(): void {
    this.setMarker();
  }
  public setMarker() {
    const position = new google.maps.LatLng(this.geoLocation.lat, this.geoLocation.long);
    const mapProperties: google.maps.MapOptions = new MapOptions(position);
    if (!this.map) {
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
    } else {
      this.map.panTo(position);
      this.map.setCenter(position);
    }
    if (!this.marker) {
      this.marker = new google.maps.Marker(
        {
          position,
          map: this.map,
          animation: google.maps.Animation.DROP,
        }
      );
    } else {
      this.marker.setAnimation(google.maps.Animation.BOUNCE);
      setTimeout(() => {
        this.marker.setAnimation(null);
      }, 500);
      this.marker.setPosition(position);
    }
  }

  public ngOnChanges(val: SimpleChanges) {
    if (val.geoLocation && (
      this.geoLocation && val.geoLocation.previousValue && (
        val.geoLocation.previousValue.lat !== this.geoLocation.lat ||
        val.geoLocation.previousValue.long !== this.geoLocation.long
      )
    )) {
      this.setMarker();
    }
  }
}
