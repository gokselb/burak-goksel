import { Component, Input, OnChanges, ViewChild, OnInit, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';

// Shared
import { Property, Currency } from '@re-shared/models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-property-form',
  templateUrl: './property-form.component.html'
})
export class PropertyFormComponent implements OnChanges {
  @Input() public property: Property;
  @Output() public statusChange: EventEmitter<boolean> = new EventEmitter(false);

  @ViewChild('propertyForm', { static: true }) form: NgForm;


  public currencies: Currency[] = [
    {
      name: 'Euro',
      symbol: '€',
      code: 'EUR'
    },
    {
      name: 'Pound',
      symbol: '£',
      code: 'GBP'
    },
    {
      name: 'Dollar',
      symbol: '$',
      code: 'USD'
    }
  ];

  private formSubscription: Subscription;



  public ngOnChanges(): void {
    if (this.property) {
      this.property.currency = this.currencies[1];
      if (!this.formSubscription && this.form) {
        this.statusChange.emit(false);
        this.formSubscription = this.form.statusChanges.subscribe(
          val => {
            this.statusChange.emit(val === 'INVALID' ? false : true);
          }
        );
      }
    }
  }

  public currencyChange(item: string) {
    this.property.currency = this.currencies.find(val => val.name === item.split(' - ')[0]);
  }
}
