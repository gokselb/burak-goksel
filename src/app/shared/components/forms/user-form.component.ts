import { Component, Input, ViewChild, OnChanges, Output, EventEmitter } from '@angular/core';

// 3rd
import { Subscription } from 'rxjs';

// Shared
import { User } from '@re-shared/models';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html'
})
export class UserFormComponent implements OnChanges {
  @Input() public user: User;
  @Output() public statusChange: EventEmitter<boolean> = new EventEmitter(false);
  @ViewChild('userForm', { static: true }) form: NgForm;
  private formSubscription: Subscription;

  public ngOnChanges(): void {
    if (!this.formSubscription && this.form) {
      this.statusChange.emit(false);
      this.formSubscription = this.form.statusChanges.subscribe(
        val => {
          this.statusChange.emit(val === 'INVALID' ? false : true);
        }
      );
    }
  }
}
