import { Component } from '@angular/core';

@Component({
  selector: 'app-login-layout',
  templateUrl: './login-form-layout.component.html'
})
export class LoginLayoutComponent {
  public hasAccount = true;
}
