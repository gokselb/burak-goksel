import { Component } from '@angular/core';
import { User, Roles } from '@re-shared/models';
import { AuthService } from '@re-shared/services';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html'
})
export class RegisterFormComponent {
  public user: User = {
    name: '',
    email: '',
    password: '',
    role: Roles.Client
  };
  public loading = false;

  public constructor(
    private authService: AuthService
  ) { }
  public registerUser(): void {
    this.loading = true;
    this.authService.register(this.user).subscribe(
      () => { this.loading = false; },
      () => { this.loading = false; }
    );
  }
}
