import { Component, Input, Output, EventEmitter } from '@angular/core';

// Shared
import { AuthService } from '@re-shared/services';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent {
  @Input() hasAccount: number | string;
  @Output() hasAccountChange = new EventEmitter<number>();
  public credentials = {
    email: '',
    pass: ''
  };
  public loading = false;

  public constructor(
    private authService: AuthService
  ) { }

  public login(): void {
    this.loading = true;
    this.authService.signIn(this.credentials.email, this.credentials.pass).subscribe(
      () => { this.loading = false; },
      () => { this.loading = false; }
    );
  }
}
