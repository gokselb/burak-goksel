import { Component, Input } from '@angular/core';

// 3rd
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html'
})
export class ConfirmationModalComponent {
  @Input() public message: string;
  @Input() public title: string;
  @Input() public modalRef: NgbModalRef;
}
