import { Component, ViewChild, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

// 3rd
// import { } from 'googlemaps';
import { GeoLocation } from '@re-shared/models';
import { MapOptions } from '@re-shared/consts';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html'
})
export class LocationPickerComponent implements OnInit, OnChanges {

  // public geoLocationValue: GeoLocation;
  public mapLoading = false;

  @Output() latChange = new EventEmitter<number>();
  @Output() longChange = new EventEmitter<number>();
  @Input() lat: number;
  @Input() long: number;

  @ViewChild('map', { static: true }) mapElement: any;
  map: google.maps.Map;

  private marker: google.maps.Marker;

  public ngOnInit(): void {
    this.mapLoading = true;
    if (this.lat && this.long) {
      this.buildMap(this.lat, this.long);
    } else {
      this.requestLocation();
    }
  }

  public ngOnChanges(val: SimpleChanges) {
    if (
      ((this.lat && val.lat) &&
        (val.lat.previousValue !== this.lat)) ||
      ((this.long && val.long) &&
        (val.long.previousValue !== this.long))
    ) {
      this.setMarker(new google.maps.LatLng(this.lat, this.long));
    }
  }

  private requestLocation(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(val => {
        this.buildMap(val.coords.latitude, val.coords.longitude);
      }, () => {
        this.buildMap(48.1312482, 4.1657236, 5);
      });
    }
  }

  private buildMap(lat: number, long: number, zoom?: number): void {
    this.lat = lat;
    this.long = long;
    this.latChange.emit(this.lat);
    this.longChange.emit(this.long);
    const position = new google.maps.LatLng(lat, long);
    const mapProperties: google.maps.MapOptions = new MapOptions(position, zoom, true);
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
    if (!zoom) {
      this.marker = new google.maps.Marker(
        {
          position,
          map: this.map,
          animation: google.maps.Animation.DROP,
        }
      );
    }
    this.watchClick();
  }

  private watchClick(): void {
    this.map.addListener('tilesloaded', () => {
      this.mapLoading = false;
    });
    this.map.addListener('click', (val) => {
      this.setMarker(val.latLng);
      this.lat = val.latLng.lat();
      this.long = val.latLng.lng();
      this.latChange.emit(this.lat);
      this.longChange.emit(this.long);
    });
  }

  private setMarker(geoLocation: google.maps.LatLng): void {
    if (!this.marker) {
      this.marker = new google.maps.Marker(
        {
          position: geoLocation,
          map: this.map,
          animation: google.maps.Animation.DROP,
        }
      );
    } else if (geoLocation !== this.marker.getPosition()) {
      this.marker.setAnimation(google.maps.Animation.BOUNCE);
      this.marker.setPosition(geoLocation);
      this.map.setZoom(15);
      this.map.panTo(geoLocation);
      setTimeout(() => {
        this.marker.setAnimation(null);
      }, 500);
    }
  }
}
