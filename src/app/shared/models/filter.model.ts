import { User } from './user.model';

export interface Filter {
  field: string;
  operator: Operators;
  value?: string | number | User;
}

export enum FilterTypes {
  Size = 0,
  Price = 1,
  Room = 2
}


export enum Operators {
  Equals = 0,
  Bigger = 1,
  Smaller = 2,
  Realtor = 3
}
