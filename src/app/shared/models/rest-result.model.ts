export interface RestResult<T> {
  documents: T;
}
