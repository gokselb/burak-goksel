export enum Roles {
  Admin = 0,
  Realtor = 1,
  Client = 2,
}