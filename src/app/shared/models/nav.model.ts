import { Roles } from './roles.enum';

export interface Nav {
  title: string;
  url: string;
  roles: Roles[];
}