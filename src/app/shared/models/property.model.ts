// name,
// description,
// floor area size,
// price per month,
// number of rooms,
// valid geolocation coordinates,
// date added and an associated realtor.

import { Currency } from './currency.model';
import { GeoLocation } from './geo-location.model';
import { State } from './state.enum';
import { User } from './user.model';
import { BaseModel } from './base.model';

export interface Property extends BaseModel {
  name: string;
  description: string;
  size: number;
  price: number;
  currency: Currency;
  rooms: number;
  geoLocation: GeoLocation;
  dateAdded: Date;
  dateRented: Date;
  state: State;
  realtor: User;
}
