import { BaseModel } from './base.model';
import { Roles } from './roles.enum';

export interface User extends BaseModel {
  name: string;
  email: string;
  password: string;
  role: Roles;
}
