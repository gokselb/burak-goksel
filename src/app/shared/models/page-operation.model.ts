export interface PageOperation {
  title: string;
  operation: () => void;
}
