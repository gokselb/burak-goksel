import { Injectable } from '@angular/core';

// Shared
import { User } from '@re-shared/models';

// Local
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserApiService extends BaseHttpService<User> {
  public constructor(
    protected http: HttpClient
  ) {
    super('user', http);
  }
}
