import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Shared
import { Property } from '@re-shared/models';

// Local
import { BaseHttpService } from './base-http.service';

@Injectable()
export class PropertyApiService extends BaseHttpService<Property> {
  public constructor(
    protected http: HttpClient
  ) {
    super('property', http);
  }
}
