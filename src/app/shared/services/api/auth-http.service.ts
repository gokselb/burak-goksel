import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// 3rd
import { Observable } from 'rxjs';

// Shared
import { User } from '@re-shared/models';

// Local
import { environment } from 'src/environments/environment';
import { Endpoints } from '../../consts';
import { UserApiService } from './user-http.service';
import { UtilsService } from '../utils.service';

@Injectable()
export class AuthApiService {
  private BASE_URL = 'https://identitytoolkit.googleapis.com/v1/';
  public constructor(
    private http: HttpClient
  ) {
  }

  public signIn(email: string, password: string): Observable<any> {
    return this.http.post<any>(this.BASE_URL + Endpoints.SigninWithPassword,
      {
        email,
        password,
        returnSecureToken: true
      },
      {
        params:
        {
          key: environment.firebase.apiKey
        }
      }
    );
  }

  public register(user: User): Observable<any> {
    return this.http.post<any>(this.BASE_URL + Endpoints.SignUp, {
      email: user.email,
      password: user.password,
      displayName: user.name
    }, {
      params: {
        key: environment.firebase.apiKey
      }
    });
  }

  public sendOob(user: User): Observable<any> {
    return this.http.post<any>(this.BASE_URL + Endpoints.SendOObCode, {
      requestType: 'PASSWORD_RESET',
      email: user.email
    }, {
      params: {
        key: environment.firebase.apiKey
      }
    });
  }

  public lookup(email: string, token: string): Observable<any> {
    return this.http.post<any>(this.BASE_URL + Endpoints.Lookup, {
      idToken: token,
      email: [email]
    }, {
      params: {
        key: environment.firebase.apiKey
      }
    });
  }

  public delete(idToken: string): Observable<any> {
    return this.http.post<any>(this.BASE_URL + Endpoints.Delete, { idToken }, {
      params: {
        key: environment.firebase.apiKey
      }
    });
  }

  public update(idToken: string, user: User): Observable<any> {
    return this.http.post<any>(this.BASE_URL + Endpoints.Update,
      {
        idToken,
        email: user.email,
        displayName: user.name
      }, {
      params: {
        key: environment.firebase.apiKey
      }
    });
  }
}
