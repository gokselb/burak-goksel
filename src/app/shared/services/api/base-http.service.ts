import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

// Shared
import { BaseModel, } from '@re-shared/models';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

export class BaseHttpService<T extends BaseModel> {
  public BASE_URL = `https://${environment.firebase.projectId}.firebaseio.com`;
  public constructor(
    public endPoint: string,
    protected http: HttpClient
  ) {
  }

  getById(id: string): Observable<T> {
    return this.http.get<T>(`${this.BASE_URL}/${this.endPoint}/${id}.json`);
  }

  getAll(): Observable<T[]> {
    return this.http.get<object>(`${this.BASE_URL}/${this.endPoint}.json`).pipe(
      map(this.mapId));
  }

  create(item: T): Observable<T> {
    return this.http.put<T>(`${this.BASE_URL}/${this.endPoint}/${item.id}.json`, item);
  }

  getAllByField(field: string, value: string): Observable<T[]> {
    return this.http.get<T>(`${this.BASE_URL}/${this.endPoint}.json`, {
      params: {
        orderBy: JSON.stringify(field),
        equalTo: JSON.stringify(value)
      }
    }).pipe(
      map(this.mapId)
    );
  }

  update(item: T): Observable<T> {
    return this.http.put<T>(`${this.BASE_URL}/${this.endPoint}/${item.id}.json`, item);
  }

  delete(id: string): Observable<boolean> {
    return this.http.delete<boolean>(`${this.BASE_URL}/${this.endPoint}/${id}.json`);
  }


  private mapId(val: any): T[] {
    const result: T[] = [];
    Object.keys(val).forEach(id => {
      let item: T;
      item = val[id];
      item.id = id;
      result.push(item);
    });
    return result;
  }
}
