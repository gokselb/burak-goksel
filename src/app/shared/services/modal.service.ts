import { Injectable } from '@angular/core';

// 3rd
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalComponent } from '@re-shared/components/confirmation-modal/confirmation-modal.component';

@Injectable()
export class ModalService {

  public constructor(
    private nbgModal: NgbModal
  ) { }

  public show(content: any): NgbModalRef {
    const modalRef = this.nbgModal.open(content, {
      windowClass: 'modal-card',
      size: 'lg'
    });
    if (modalRef.componentInstance) {
      modalRef.componentInstance.modalRef = modalRef;
    }
    return modalRef;
  }

  public showConfirmationModal(message: string = 'This action cannot be undone.', title: string = 'Are you sure?'): NgbModalRef {
    const ref = this.show(ConfirmationModalComponent);
    ref.componentInstance.message = message;
    ref.componentInstance.title = title;
    return ref;
  }
}
