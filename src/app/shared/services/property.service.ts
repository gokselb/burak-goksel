import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

// 3rd
import { BehaviorSubject, Observable, throwError } from 'rxjs';

// Shared
import { Property, Filter, Operators, Roles, State, User } from '@re-shared/models';
import { map, catchError, filter } from 'rxjs/operators';

// Local
import { PropertyApiService } from './api';
import { NotifierService } from './notifier.service';
import { UtilsService } from './utils.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  public visiblePorperties: BehaviorSubject<Property[]> = new BehaviorSubject(null);
  public isMap = new BehaviorSubject(false);

  private allProperties: Property[];
  private filters: Filter[] = [];
  public constructor(
    private propertyApiService: PropertyApiService,
    private notifier: NotifierService,
    private utilsService: UtilsService,
    private router: Router,
    private authService: AuthService
  ) {
    this.getProperties().subscribe();
    this.watchRoute();
    this.watchUser();
  }

  public getProperties(): Observable<void> {
    return this.propertyApiService.getAll().pipe(
      map(properties => {
        properties.map(property => {
          property.dateAdded = property.dateAdded ? new Date(property.dateAdded) : property.dateAdded;
          property.dateRented = property.dateRented ? new Date(property.dateRented) : property.dateRented;
        });
        return properties;
      }),
      map((val => {
        this.allProperties = val;
        this.updateVisiblePorperties();
      }
      ))
    );
  }

  public create(property: Property): Observable<Property> {
    property.dateAdded = new Date();
    property.id = this.utilsService.generateGuid();
    return this.propertyApiService.create(property).pipe(
      map(val => {
        this.notifier.success('Property added succesfully.');
        return val;
      }),
      catchError(err => {
        this.notifier.error();
        return throwError(err);
      })
    );
  }

  public delete(id: string): Observable<boolean> {
    return this.propertyApiService.delete(id).pipe(
      map(val => {
        this.notifier.success('Property deleted permanently.');
        return val === null ? true : false;
      }),
      catchError(err => {
        this.notifier.error();
        return throwError(err);
      })
    );
  }

  public update(property: Property, showInfo = true) {
    return this.propertyApiService.update(property).pipe(
      map(val => {
        this.allProperties = this.allProperties.map(prop => {
          if (prop.id === property.id) {
            return property;
          }
          return prop;
        });
        this.updateVisiblePorperties();
        if (showInfo) {
          this.notifier.success('Property updated successfully.');
        }
        return val;
      }),
      catchError(err => {
        this.notifier.error();
        return throwError(err);
      })
    );
  }

  public updateVisiblePorperties(): void {
    if (this.allProperties) {
      this.allProperties.sort((a, b) => b.dateAdded.getTime() - a.dateAdded.getTime());
      const clonedProperties: Property[] = [];
      this.allProperties.forEach(val => clonedProperties.push(Object.assign({}, val)));
      let result = clonedProperties.map<null | Property>(val => {
        this.filters.forEach(filter => {
          if (val) {
            if (
              filter.operator === Operators.Equals &&
              filter.value !== val[filter.field]
            ) {
              val = null;
            }
            if (
              filter.operator === Operators.Bigger &&
              filter.value < val[filter.field]
            ) {
              val = null;
            }
            if (
              filter.operator === Operators.Smaller &&
              filter.value > val[filter.field]
            ) {
              val = null;
            }
            if (
              filter.operator === Operators.Realtor &&
              (filter.value as User).id !== val.realtor.id
            ) {
              val = null;
            }
          }
        });
        return val;
      });
      result = result.filter(val => val);
      // Update this visible properties if new value is different.
      const same = JSON.stringify(result) === JSON.stringify(this.visiblePorperties.value);
      if (!same) {
        this.visiblePorperties.next(result);
      }
    }
  }

  public registerFilter(filter: Filter) {
    const index = this.filters.findIndex(val => val.field === filter.field && val.operator === filter.operator);
    if (index > -1) {
      this.filters[index].value = filter.value;
      if (!filter.value) {
        this.removeFilter(filter);
      }
    } else {
      this.filters.push(filter);
    }
    this.updateVisiblePorperties();
  }

  public removeFilter(filter: Filter) {
    const filterIndex = this.filters.findIndex(val => val.field === filter.field && val.operator === filter.operator);
    if (filterIndex > -1) {
      this.filters.splice(filterIndex, 1);
    }
    this.updateVisiblePorperties();
  }

  public removeAllFilter() {
    this.filters = [];
    this.updateVisiblePorperties();
  }

  private watchRoute(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      this.isMap.next(this.router.url.indexOf('map') > -1);
    });
  }

  private watchUser(): void {
    this.authService.user.subscribe(user => {
      if ((user && user.role === Roles.Client) || user === null) {
        this.registerFilter(
          {
            field: 'state',
            operator: Operators.Equals,
            value: State.Available
          }
        );
      } else {
        this.removeFilter(
          {
            field: 'state',
            operator: Operators.Equals
          }
        );
      }
      this.updateVisiblePorperties();
    });
  }
}
