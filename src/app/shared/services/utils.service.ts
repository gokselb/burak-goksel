import { Injectable } from '@angular/core';
import { BaseModel } from '@re-shared/models';

@Injectable()
export class UtilsService {
  public generateGuid(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      var r =
        Math.random() * 16
        | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  public getErrorMessage(error: string): string {
    if (error === 'EMAIL_EXISTS') {
      return 'The email address you have entered is already registered.';
    }
    if (error === 'INVALID_PASSWORD' || error === 'INVALID_EMAIL') {
      return 'Invalid username or password.';
    }
  }

}
