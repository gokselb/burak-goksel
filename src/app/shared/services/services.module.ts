import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// 3rd
import { ToastrModule } from 'ngx-toastr';

// Local
import { UtilsService } from './utils.service';
import { PropertyApiService, AuthApiService, UserApiService } from './api';
import { NotifierService } from './notifier.service';
import { NavService } from './navigation/nav.service';
import { PageOperationService } from './page-operation.service';
import { ModalService } from './modal.service';
import { PropertyService } from './property.service';

@NgModule({
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()

  ],
  declarations: [],
  providers: [
    UtilsService,
    PropertyApiService,
    NotifierService,
    AuthApiService,
    UserApiService,
    NavService,
    PageOperationService,
    ModalService
  ]
})
export class ServicesModule { }
