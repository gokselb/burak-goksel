import { Injectable } from '@angular/core';
import { User } from '@re-shared/models';
import { BehaviorSubject, Observable, forkJoin, concat, merge, throwError } from 'rxjs';
import { AuthApiService, UserApiService } from './api';
import { UtilsService } from './utils.service';
import { NotifierService } from './notifier.service';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: BehaviorSubject<User> = new BehaviorSubject(null);
  public userSearch: BehaviorSubject<string> = new BehaviorSubject(null);
  public token: BehaviorSubject<string> = new BehaviorSubject(null);
  public constructor(
    private authApiService: AuthApiService,
    private userApiService: UserApiService,
    private notify: NotifierService,
    private utilsService: UtilsService,
    private router: Router
  ) {
    this.getInfoFromStorage();
  }

  public signIn(email: string, password: string): Observable<any> {
    return this.authApiService.signIn(email, password).pipe(
      map((val) => {
        this.handleLoginResult(val, email);
        return val;
      }),
      catchError((err) => {
        this.notify.error(this.utilsService.getErrorMessage(err.error.error.message));
        return throwError(err);
      })
    );
  }
  public register(user: User): Observable<any> {
    user.id = this.utilsService.generateGuid();
    return this.authApiService.register(user).pipe(
      map(val => {
        this.handleRegisterResult(val, user);
        return val;
      }),
      catchError(err => {
        this.notify.error(this.utilsService.getErrorMessage(err.error.error.message));
        return throwError(err);
      }
      )
    );
  }

  public logout(): void {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.user.next(null);
    this.token.next(null);
    this.router.navigateByUrl('/');
  }

  public createUser(user: User): Observable<boolean> {
    return new Observable(subs => {
      user.id = this.utilsService.generateGuid();
      user.password = this.utilsService.generateGuid();
      let success = false;
      concat(
        this.authApiService.register(user),
        this.userApiService.create(user),
        this.authApiService.sendOob(user)
      ).subscribe(
        () => {
          if (!success) {
            success = true;
            this.notify.success('User created, email reset link sent.');
          }
        },
        (err) => {
          subs.error(err);
          this.notify.error(this.utilsService.getErrorMessage(err.error.error.message));
        },
        () => {
          subs.next(true);
        }
      );
    });
  }

  public deleteUser(user: User): Observable<boolean> {
    return new Observable(subs => {
      let success = false;
      this.authApiService.signIn(user.email, user.password).subscribe(
        result => {
          merge(
            this.authApiService.delete(result.idToken),
            this.userApiService.delete(user.id)
          ).subscribe(
            () => {
              if (!success) {
                subs.next(true);
                success = true;
                this.notify.success('User deleted permanently.');
              }
            },
            (err) => {
              subs.error(err);
              this.notify.error(this.utilsService.getErrorMessage(err.error.error.message));
            }
          );
        },
        (err) => {
          subs.error(err);
          this.notify.error(this.utilsService.getErrorMessage(err.error.error.message));
        }
      );
    });
  }

  public updateUser(user: User, newUser: User): Observable<User> {
    return new Observable(subs => {
      let success = false;
      this.authApiService.signIn(user.email, user.password).subscribe(
        result => {
          merge(
            this.authApiService.update(result.idToken, newUser),
            this.userApiService.update(newUser)
          ).subscribe(
            () => {
              if (!success) {
                subs.next(newUser);
                success = true;
                this.notify.success('User updated.');
              }
            },
            (err) => {
              subs.error(err);
              this.notify.error(this.utilsService.getErrorMessage(err.error.error.message));
            }
          );
        },
        (err) => {
          subs.error(err);
          this.notify.error(this.utilsService.getErrorMessage(err.error.error.message));
        }
      );
    });
  }

  public resetPassword(user: User): Observable<any> {
    return this.authApiService.sendOob(user).pipe(
      map(val => {
        if (val && val.email) {
          this.notify.success('Email sent successfully.');
        }
      }),
      catchError(err => {
        this.notify.error(err);
        return throwError(err);
      })
    );
  }

  private getInfoFromStorage(): void {
    const user = JSON.parse(localStorage.getItem('user')) as User;
    const token = localStorage.getItem('token');
    if (user) {
      this.user.next(user);
    }
    if (token) {
      this.token.next(token);
    }
  }

  private handleLoginResult(result: any, email: string): void {
    localStorage.setItem('token', result.idToken);
    this.token.next(result.idToken);
    this.userApiService.getAllByField('email', email).subscribe(user => {
      localStorage.setItem('user', JSON.stringify(user[0]));
      this.user.next(user[0]);
    });
  }

  private handleRegisterResult(result: any, user: User): void {
    localStorage.setItem('token', result.idToken);
    this.token.next(result.idToken);
    this.userApiService.create(user).subscribe(createdUser => {
      localStorage.setItem('user', JSON.stringify(createdUser));
      this.user.next(createdUser);
    });
  }
}
