import { Injectable } from '@angular/core';

// 3rd
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotifierService {
  public constructor(
    private toastr: ToastrService
  ) { }
  public success(message: string): void {
    this.toastr.success(message);
  }
  public error(message: string = 'An error occurred, please try again.'): void {
    if (typeof message === 'string') {
      this.toastr.error(message);
    }
  }
}
