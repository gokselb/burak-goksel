import { Nav } from '@re-shared/models/nav.model';
import { Roles } from '@re-shared/models';

export const navItems: Nav[] = [
  {
    title: 'List',
    url: '/',
    roles: [
      Roles.Admin,
      Roles.Realtor,
      Roles.Client
    ]
  },
  {
    title: 'Map',
    url: '/map',
    roles: [
      Roles.Admin,
      Roles.Realtor,
      Roles.Client
    ]
  },
  {
    title: 'Users',
    url: '/users',
    roles: [
      Roles.Admin
    ]
  }
];
