import { Injectable } from '@angular/core';

// 3rd
import { BehaviorSubject } from 'rxjs';

// Shared
import { Nav } from '@re-shared/models/nav.model';
import { Roles } from '@re-shared/models';

// Local
import { AuthService } from '../auth.service';
import { navItems } from './nav.item';

@Injectable()
export class NavService {
  public navs: BehaviorSubject<Nav[]> = new BehaviorSubject(null);
  public constructor(
    private authService: AuthService
  ) {
    this.watchUser();
  }
  public watchUser(): void {
    this.authService.user.subscribe(user => {
      if (user) {
        this.navs.next(navItems.filter(nav => nav.roles.indexOf(user.role) !== -1));
      } else {
        this.navs.next(navItems.filter(nav => nav.roles.indexOf(Roles.Client) !== -1));
      }
    });
  }
}
