import { Injectable } from '@angular/core';

// 3rd
import { BehaviorSubject } from 'rxjs';
import { PageOperation } from '@re-shared/models';
import { AuthService } from './auth.service';

@Injectable()
export class PageOperationService {
  public operations: BehaviorSubject<PageOperation[]> = new BehaviorSubject(null);
  public constructor(
    private auth: AuthService
  ) { }
  public registerOperations(operations: PageOperation[]): void {
    this.operations.next(operations);
  }

  private logout(): void {
    this.auth.logout();
  }
}
