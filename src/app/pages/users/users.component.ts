import { Component } from '@angular/core';

// Shared
import { UserApiService } from '@re-shared/services/api';
import { User } from '@re-shared/models';
import { AuthService } from '@re-shared/services';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent {
  public users: User[];
  public newUser: User;
  public newUserLoading = false;
  public userSearch: string = null;
  public formValid = false;

  public constructor(
    private usersService: UserApiService,
    private authService: AuthService
  ) {
    this.getUsers().subscribe();
    this.watchSearch();
  }
  public createUser(): void {
    this.newUserLoading = true;
    const usersCopy = this.users;
    this.users = null;
    this.authService.createUser(this.newUser).subscribe(val => {
      if (val) {
        this.newUser = null;
        this.getUsers().subscribe();
      }
      this.newUserLoading = false;
    },
      () => {
        this.users = usersCopy;
        this.newUserLoading = false;
      }
    );
  }

  public newUserClicked(): void {
    this.newUser = {
      name: '',
      email: '',
      password: '',
      role: null
    };
  }

  private getUsers() {
    return this.usersService.getAll().pipe(map(val => {
      this.users = val;
      return val;
    }));
  }

  private watchSearch(): void {
    this.authService.userSearch.subscribe(val => {
      this.userSearch = val;
    });
  }
}
