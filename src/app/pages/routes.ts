import { Routes } from '@angular/router';

// Local
import { PropertiesComponent } from './properties/properties.component';
import { LayoutComponent } from './layout/layout.component';
import { UsersComponent } from './users/users.component';
import { ReAuthGuard } from '@re-shared/security';
import { PropertiesListComponent } from './properties/list/list.component';
import { PropertiesMapComponent } from './properties/map/map.component';


export const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'users',
        component: UsersComponent,
        canActivate: [ReAuthGuard]
      },
      {
        path: '',
        component: PropertiesComponent,
        canActivate: [ReAuthGuard],
        children: [
          {
            path: '',
            canActivate: [ReAuthGuard],
            component: PropertiesListComponent
          },
          {
            path: 'map',
            canActivate: [ReAuthGuard],
            component: PropertiesMapComponent
          },
          { path: '**', redirectTo: '' }
        ]
      },
      { path: '**', redirectTo: '' }
    ]
  }
];
