import { Component, Input, OnInit, ViewChild, OnChanges } from '@angular/core';

// Shared
import { Property, User, Roles, Operators, State } from '@re-shared/models';
import { MapOptions } from '@re-shared/consts';
import { PropertyService, AuthService } from '@re-shared/services';

@Component({
  selector: 'app-properties-map',
  templateUrl: './map.component.html'
})
export class PropertiesMapComponent implements OnInit, OnChanges {
  public properties: Property[] = [];
  public cudEnabled = false;

  @ViewChild('map', { static: true }) mapElement: any;
  map: google.maps.Map;

  public mapLoading = false;
  public selectedProperty: Property;
  public selectedMarker: google.maps.Marker;

  private user: User;
  private markers: google.maps.Marker[] = [];
  private bounds = new google.maps.LatLngBounds();

  public constructor(
    private propertyService: PropertyService,
    private authService: AuthService
  ) { }

  public ngOnInit(): void {
    this.buildMap();
    this.watchProperties();
    this.watchUser();
  }

  public ngOnChanges(): void {
    if (this.map) {
      this.setMarkers();
    }
  }
  private watchProperties(): void {
    this.propertyService.visiblePorperties.subscribe(properties => {
      if (properties) {
        this.properties = properties;
        if (this.selectedProperty) {
          this.selectedProperty = this.properties.find(val => val.id === this.selectedProperty.id);
        }
        this.setMarkers();
      }
    });
  }

  private buildMap(): void {
    const mapProperties: google.maps.MapOptions = new MapOptions(null, 0, true);
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
  }

  private setMarkers(): void {

    this.markers.map(val => val.setMap(null));
    this.markers = [];

    this.properties.forEach(val => {
      const marker = new google.maps.Marker(
        {
          position: new google.maps.LatLng(val.geoLocation.lat, val.geoLocation.long),
          map: this.map,
          animation: google.maps.Animation.DROP,
        }
      );
      this.bounds.extend(marker.getPosition());
      marker.addListener('click', () => { this.markerClicked(val, marker); });
      this.markers.push(marker);
    });
    if (!this.selectedProperty) {
      this.map.fitBounds(this.bounds, 200);
    } else {
      this.markerClicked(this.selectedProperty, this.selectedMarker);
    }
  }

  private markerClicked(property: Property, marker: google.maps.Marker): void {
    this.selectedProperty = property;
    this.selectedMarker = marker;
    marker.setAnimation(google.maps.Animation.BOUNCE);
    const position = new google.maps.LatLng(property.geoLocation.lat, property.geoLocation.long);
    const currentZoom = this.map.getZoom();
    this.map.setZoom(currentZoom < 14 ? 14 : currentZoom > 16 ? currentZoom : currentZoom + 1);
    this.map.panTo(position);
    setTimeout(() => {
      marker.setAnimation(null);
    }, 500);
  }
  private watchUser(): void {
    this.authService.user.subscribe(user => {
      this.user = user;
      this.cudEnabled = this.user && this.user.role !== Roles.Client;
    });
  }

  public closeInfoPanel() {
    this.selectedProperty = null;
    this.selectedMarker = null;
    this.map.fitBounds(this.bounds, 200);
  }
}
