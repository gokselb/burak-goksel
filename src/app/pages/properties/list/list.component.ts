import { Component, Input } from '@angular/core';

// Shared
import { Property, User, State, Roles } from '@re-shared/models';
import { PropertyService, AuthService } from '@re-shared/services';

@Component({
  selector: 'app-properties-list',
  templateUrl: './list.component.html'
})
export class PropertiesListComponent {
  public properties: Property[];
  public cudEnabled = false;
  public newPropertyLoading = false;
  public newProperty: Property;
  public formValid = false;
  private user: User;

  public constructor(
    private propertyService: PropertyService,
    private authService: AuthService
  ) {
    this.getProperties();
    this.watchUser();
  }
  public createProperty(): void {
    this.newPropertyLoading = true;
    this.propertyService.create(this.newProperty).subscribe(
      () => {
        this.propertyService.getProperties().subscribe();
        this.newProperty = null;
      },
      () => null,
      () => this.newPropertyLoading = false
    );
  }
  public newPropertyClicked(): void {
    this.newProperty = {
      name: null,
      description: null,
      size: null,
      price: null,
      currency: null,
      rooms: null,
      geoLocation: {
        lat: null,
        long: null
      },
      dateAdded: null,
      dateRented: null,
      state: State.Available,
      realtor: this.user
    };
  }

  private getProperties(): void {
    this.propertyService.getProperties().subscribe();
    this.propertyService.visiblePorperties.subscribe(val => {
      this.properties = val;
    });
  }

  private watchUser(): void {
    this.authService.user.subscribe(user => {
      this.user = user;
      this.cudEnabled = this.user && this.user.role !== Roles.Client;
    });
  }
}
