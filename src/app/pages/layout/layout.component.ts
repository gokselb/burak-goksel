import { Component, OnInit } from '@angular/core';
import { AuthService } from '@re-shared/services/auth.service';
import { User } from '@re-shared/models';
import { PropertyService } from '@re-shared/services';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html'
})
export class LayoutComponent implements OnInit {
  public user: User;
  public isMap = false;
  public constructor(
    private authService: AuthService,
    private properyService: PropertyService
  ) {

  }
  public ngOnInit(): void {
    this.watchUser();
    this.watchMap();
  }

  private watchUser(): void {
    this.authService.user.subscribe(val => {
      this.user = val;
    });
  }

  private watchMap(): void {
    this.properyService.isMap.subscribe(val => {
      this.isMap = val;
    });
  }
}
