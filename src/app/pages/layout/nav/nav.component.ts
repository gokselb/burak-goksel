import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

// 3rd
import { filter } from 'rxjs/operators';

// Shared
import { NavService, AuthService } from '@re-shared/services';
import { Nav, User } from '@re-shared/models';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html'
})
export class NavComponent implements OnInit {
  public navs: Nav[];
  public activeUrl = '/';
  public user: User;

  public constructor(
    private navService: NavService,
    private router: Router,
    private auth: AuthService
  ) { }

  public ngOnInit(): void {
    this.watchNavChange();
    this.watchUser();
  }
  private watchNavChange() {
    this.activeUrl = this.router.url;
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      this.activeUrl = this.router.url;
    });
    this.navService.navs.subscribe(navs => {
      if (navs) {
        this.navs = navs;
      }
    });
  }

  private watchUser(): void {
    this.auth.user.subscribe(val => {
      this.user = val;
    })
  }

  public logout(): void {
    this.auth.logout();
  }
}
