import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

// 3rd
import { filter } from 'rxjs/operators';

// Shared
import { PropertyService, AuthService } from '@re-shared/services';
import { Filter, Operators, User, Roles } from '@re-shared/models';

@Component({
  selector: 'app-page-operations',
  templateUrl: './page-operations.component.html'
})
export class PageOperationsComponent {
  public isUserPage = false;
  public userSearch: string;
  public isMap = false;
  public isClient = true;
  public onlyMy = false;

  public filters = {
    size: {
      min: {
        field: 'size',
        operator: Operators.Smaller,
        value: null
      } as Filter,
      max: {
        field: 'size',
        operator: Operators.Bigger,
        value: null
      } as Filter
    },
    price: {
      min: {
        field: 'price',
        operator: Operators.Smaller,
        value: null
      } as Filter,
      max: {
        field: 'price',
        operator: Operators.Bigger,
        value: null
      } as Filter
    },
    rooms: {
      min: {
        field: 'rooms',
        operator: Operators.Smaller,
        value: null
      } as Filter,
      max: {
        field: 'rooms',
        operator: Operators.Bigger,
        value: null
      } as Filter
    }
  };

  private user: User;

  public constructor(
    private router: Router,
    private propertyService: PropertyService,
    private authService: AuthService
  ) {
    this.watchRoute();
    this.watchUser();
  }

  public updateFilters(updateFilter: Filter): void {
    this.propertyService.registerFilter(updateFilter);
  }

  private watchUser(): void {
    this.authService.user.subscribe(user => {
      this.user = user;
      this.isClient = (user && user.role === Roles.Client) || !user;
    });
  }
  private watchRoute(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      this.isUserPage = this.router.url.indexOf('user') > -1;
      this.isMap = this.router.url.indexOf('map') > -1;
    });
  }

  public searchUser(): void {
    this.authService.userSearch.next(this.userSearch);
  }

  public showOnlyRealtors() {
    if (this.onlyMy) {
      this.propertyService.registerFilter({
        field: 'realtor',
        operator: Operators.Realtor,
        value: this.user
      });
    } else {
      this.propertyService.removeFilter({
        field: 'realtor',
        operator: Operators.Realtor
      });
    }
  }
}
