import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Shared
import { SharedModule } from '@re-shared';

// Local
import { routes } from './routes';
import { PropertiesComponent } from './properties/properties.component';
import { LayoutComponent } from './layout/layout.component';
import { TopBarComponent } from './layout/top-bar/top-bar.component';
import { NavComponent } from './layout/nav/nav.component';
import { UsersComponent } from './users/users.component';
import { PageOperationsComponent } from './layout/page-operations/page-operations.component';
import { PropertiesListComponent } from './properties/list/list.component';
import { PropertiesMapComponent } from './properties/map/map.component';

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    SharedModule,
    CommonModule,
    FormsModule
  ],
  declarations: [
    LayoutComponent,
    PropertiesComponent,
    PropertiesListComponent,
    PropertiesMapComponent,
    UsersComponent,
    PageOperationsComponent,
    TopBarComponent,
    NavComponent,
  ],
  providers: [],

})
export class PagesModule { }
